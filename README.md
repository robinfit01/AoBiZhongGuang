# 奥比中光
之前了解过体感摄像头，今天将搜索到的资料进行汇总，以方便后来者。（资料均为公开的，所以不涉及到版权等问题）


```
.
├── Android3D体感测试游戏
│   └── 8月更新
│       ├── AnimalKindom_v0.1.27__20160816_Orbbec.apk
│       ├── ChildBook1.0.63.apk
│       ├── ChildTakePhoto_v1.0.0_20160816_640_Orbbec.apk
│       ├── FunTakePhoto_v1.0.0_20160816_640_Orbbec.apk
│       ├── MagicSalad_v1.8.0_20160812_Orbbec.apk
│       ├── OrbbecMusic_v1.1.0_20160816_Orbbec.apk
│       ├── SaveSheep_1.8.0_20160816_COMMON.apk
│       └── Wukong_1.0.0_20160816_COMMON.apk
├── README.md
├── 奥比中光3D体感摄像头介绍
│   ├── ASTRA Mini产品规格_new.pdf
│   ├── ASTRA S产品规格.pdf
│   ├── ASTRA 产品规格.pdf
│   └── 奥比中光3D传感设备解决方案介绍.pdf
├── 奥比中光SDK
│   ├── android
│   │   └── android_sdk_1.1.4.rar
│   └── astra SDK
│       ├── AstraSDKUnity536-v2.0.7-beta-5f803a5e24.unitypackage
│       ├── AstraSDKUnity554-v2.0.7-beta-5f803a5e24.unitypackage
│       ├── ASTRASDK开发文档-v2.0.7-beta .pdf
│       ├── AstraUnity536Sample-v2.0.7-beta-5f803a5e24.apk
│       └── AstraUnity554Sample-v2.0.7-beta-5f803a5e24.apk
└── 奥比中光资料汇总说明.docx
6 directories, 20 files

```
